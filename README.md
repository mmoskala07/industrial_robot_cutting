﻿# Cutting in metal sheet by industrial robot

## Paper
[Project Description](Projekt_Robotyka.pdf)

## Crucial variables
![Coordinate_system|small](coordinate_system.png)  
`P_LU` = (x, y)  
`P_RD` = (x, y)  
In case of bad coordinates robot will do nothing  
`CHAR_NUM` = INT  
`GOSUB * WRITE_<letter>`  
The number of chars must be the same as amount of `GOSUB * WRITE_<letter>` instruction occurs.  

## Video
![Working process](working.mp4)

## Maintainer
Mateusz Moskała  
mmoskala07@gmail.com  
